
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;



//https://probinson1996@bitbucket.org/probinson1996/gadgetman.git
//http://www.macs.hw.ac.uk/guidebook/?name=Using%20The%20GUI&page=1

public class GadgetView
{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int FRAME_WIDTH = (int) (screenSize.getWidth() * 0.9);
		int FRAME_HEIGHT = (int) (screenSize.getHeight() * 0.9);
        private JFrame Frame;
        private JPanel mainPanel; 
        private JLabel characterLabel, mapLabel, gadgetLabel;
        private int a, b;
        public GadgetView()
        {
        	Frame = new JFrame("Gadget Man v1.0");
            Frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
            Frame.setContentPane(initalizeView());
            Frame.setVisible(true);
            Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            characterAnimate();
            
        }
        

   
        private void characterAnimate() {
        	a = 1;
            b = 0;
           ActionListener task = new ActionListener(){
               public void actionPerformed(ActionEvent e) {
               	characterLabel.setLocation(characterLabel.getX(), characterLabel.getY() + a);
               	b++;
               	if (b > 1)
               	{
               		a = a * -1;
               		b = 0;
               	}
         
               }

           };
           Timer timer = new Timer(200, task);
           timer.setRepeats(true);                
           timer.start();
			
		}



		public JPanel initalizeView()
        {
        	mainPanel = new JPanel();
        	mainPanel.setLayout(null);

            mapLabel = new JLabel(new ImageIcon("MapA.png"));
            mapLabel.setLocation(0,0);
            mapLabel.setSize(FRAME_WIDTH,FRAME_HEIGHT);
           
            characterLabel = new JLabel(new ImageIcon("GadgetManE.png"));
            characterLabel.setLocation(FRAME_WIDTH / 2, FRAME_HEIGHT / 2);
            characterLabel.setSize(70,70);
           
            mainPanel.add(characterLabel);
            mainPanel.add(mapLabel);
            
            mainPanel.setOpaque(false);
            return mainPanel;
            
        }
        
        public JLabel getCharacterLabel()
        {
        	return characterLabel;
        }
        
        public void changeImage(String s)
        {
        	characterLabel = new JLabel(new ImageIcon("GadgetMan" + s + ".png"));
        	characterLabel.setHorizontalAlignment(0);
        	characterLabel.setLocation(characterLabel.getX(), characterLabel.getY());
            mainPanel.add(characterLabel);
        }
		public void addKeyListeners(KeyListener listener) {
			Frame.addKeyListener(listener);
		}
		public void addMouseListeners(MouseListener listener)
	    {       
	        Frame.addMouseListener(listener);
	    }
		public void launchRocket(int x, int y) {
			gadgetLabel = new JLabel(new ImageIcon("RocketA.gif"));
			gadgetLabel.setLocation(characterLabel.getX(), characterLabel.getY());
			gadgetLabel.setSize(70,70);
			mainPanel.add(gadgetLabel);
			mainPanel.add(characterLabel);
            mainPanel.add(mapLabel);
            
            
            System.out.println(x + "," + y);
            System.out.println(gadgetLabel.getX() + " " + gadgetLabel.getY());
         
            while (gadgetLabel.getX() != x || gadgetLabel.getY() != y)
            {
            	try {
					Thread.sleep(21);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	int differnceX = Math.abs(x - gadgetLabel.getX());
                int differnceY = Math.abs(y - gadgetLabel.getY());
                int a = -1;
                int b = -1;
                
                if (differnceX == differnceY)
                {
                	if(x > gadgetLabel.getX())
                		 a = 1;
                	if(y > gadgetLabel.getY())
                		b = 1;
                	gadgetLabel.setLocation(gadgetLabel.getX()+a, gadgetLabel.getY()+b);
                }
                else if (differnceX > differnceY && differnceY != 0)
                {
                	if(x > gadgetLabel.getX())
                		 a = 2;
                	else
                		a = -2;
                	if(y > gadgetLabel.getY())
                		b = 1;
                	gadgetLabel.setLocation(gadgetLabel.getX()+a, gadgetLabel.getY()+b);
                }
                else if (differnceX > differnceY)
                {
                	if(x > gadgetLabel.getX())
                		 a = 2;
                	else
                		a = -2;
                	gadgetLabel.setLocation(gadgetLabel.getX()+a, gadgetLabel.getY());
                }
                else if (differnceY > differnceX && differnceX != 0)
                {
                	if(x > gadgetLabel.getX())
                		 a = 1;
                	if(y > gadgetLabel.getY())
                		b = 2;
                	else
                		b = -2;
                	gadgetLabel.setLocation(gadgetLabel.getX()+a, gadgetLabel.getY()+b);
                }
                else
                {
           
                	if(y > gadgetLabel.getY())
                		b = 2;
                	else
                		b = -2;
                	gadgetLabel.setLocation(gadgetLabel.getX(), gadgetLabel.getY()+b);
                }
            }
            
		}



		
}