

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Timer;



public class GadgetController implements KeyListener, MouseListener
{
	private GadgetView View;
	private GadgetModel Model;
	
	public GadgetController(GadgetView a, GadgetModel b)
	{
		View = a;
		Model = b;
		View.addKeyListeners(this);
		View.addMouseListeners(this);
		
		
	}
	public void keyPressed(KeyEvent e) 
	{
		if (e.getKeyCode() == KeyEvent.VK_RIGHT ) 
    	{
    		
        	for (int i = 0; i < 3; i++)
        	{
        		try {
					Thread.sleep(21);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		
        		View.getCharacterLabel().setLocation(View.getCharacterLabel().getX() + 1, View.getCharacterLabel().getY());
        	}
        } 
    	else if (e.getKeyCode() == KeyEvent.VK_LEFT ) {
    		
        	for (int i = 0; i < 3; i++)
        	{
        		try {
					Thread.sleep(21);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		View.getCharacterLabel().setLocation(View.getCharacterLabel().getX() - 1, View.getCharacterLabel().getY());
        		
        	}
    	}
    	else if (e.getKeyCode() == KeyEvent.VK_UP ) {
        	
        	for (int i = 0; i < 3; i++)
        	{
        		try {
					Thread.sleep(21);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		View.getCharacterLabel().setLocation(View.getCharacterLabel().getX(), View.getCharacterLabel().getY() - 1);
        	}
  		
        	
        } 
    	else if (e.getKeyCode() == KeyEvent.VK_DOWN ) {
    		
        	for (int i = 0; i < 3; i++)
        	{
        		try {
					Thread.sleep(21);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		View.getCharacterLabel().setLocation(View.getCharacterLabel().getX(), View.getCharacterLabel().getY() + 1);
        	}
        }
		
	}
	@Override
	public void keyReleased(KeyEvent e) {

	}
	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		View.launchRocket(e.getX(), e.getY());
		System.out.println("Click registered!");
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void actionPerformed(ActionEvent e) {
	  //test
	}

	
}


